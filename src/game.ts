import {spawnGltfX, spawnEntity,spawnConeX,spawnCylinderX ,spawnBoxX, spawnPlaneX, spawnTextX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'
import {Elevator} from "./modules/elevator"
import utils from "../node_modules/decentraland-ecs-utils/index"
import { GroundCover } from './modules/GroundCover'

const scene = new Entity()
const Scenetransform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})

scene.addComponentOrReplace(Scenetransform)
engine.addEntity(scene)

const Cube = new Entity()
Cube.addComponent(new GLTFShape("models/cube_anim.gltf"))
const cubeTransform = new Transform(({
  position : new Vector3(1,1,1)

}))
Cube.addComponent(cubeTransform)
engine.addEntity(Cube)

/*

const cubeAnimator = new Animator()
cube.addComponent(cubeAnimator)
const clipCube = new AnimationState("CubeAction")
cubeAnimator.addClip(clipCube)
clipCube.play()
clipCube.looping
*/

//the fishtank

const fishTankShape = new GLTFShape("models/fishtank.glb")
const fishTank = spawnGltfX(fishTankShape, 16,0,16,  0,0,0, 1,1,1)

//the shark

const shark = new Entity()
shark.addComponent(new GLTFShape("models/shark.glb"))

const SharkTransform = new Transform(({
  scale: new Vector3(1.5, 1.5, 1.5),
  position: new Vector3(5, 2, 5)
}))

shark.addComponent(SharkTransform)

// Create the pivot entity
const pivot = new Entity()
pivot.addComponent(new Transform({
  position: new Vector3(16,4,16)
}))


engine.addEntity(pivot)
shark.setParent(pivot)
engine.addEntity(shark)


// Define a system that updates the rotation on every frame
export class PivotRotate implements ISystem {
  update() {
    let transform = pivot.getComponent(Transform)
    transform.rotate(Vector3.Down(), 3 )
  }
}

// Add the system
engine.addSystem(new PivotRotate())


const SharkAnimator = new Animator()

let animator = new Animator()
shark.addComponent(animator)
const clipSwim = new AnimationState("swim")
const clipByte = new AnimationState("byte")
animator.addClip(clipSwim)
animator.addClip(clipByte)
clipByte.play()
clipSwim.play()




export class SharkMove implements ISystem {
  update(dt: number) {
    let transform = shark.getComponent(Transform)
    let distance = Vector3.Forward.apply(dt * 3)
    transform.translate(distance)
  }
}




// Elevator level 1 height of 6m 
let elevatorHeight: number = 19.8

// Elevator position (x, y, z) with y being level 0 height at ground floor (0m)
let elevatorPosition: Vector3 = new Vector3(16, 0, 32)

// Elevator dimension (width, platform thickness, length)
let elevatorDimension: Vector3 = new Vector3(4, 0.3, 5)

// Create elevator entity
let elevator: Elevator = new Elevator(elevatorHeight, elevatorPosition, elevatorDimension)

// Add entity to engine
engine.addEntity(elevator)

//define layers
const foodLayer = 1
const mouseLayer = 2
const catLayer = 4
 
//create food
const food = new Entity()
food.addComponent(new ConeShape())
food.getComponent(ConeShape).withCollisions = false
food.addComponent(new Transform({ position: new Vector3(8 + Math.random() * 6, 20, 8 + Math.random() * 6) }))
food.addComponent(new utils.TriggerComponent(
    new utils.TriggerBoxShape(Vector3.One(), Vector3.Zero()),
    foodLayer, 
    mouseLayer | catLayer,
    () => {
    food.getComponent(Transform).position = new Vector3(8 + Math.random() * 6, 19, 8 + Math.random() * 6)
    mouse.addComponentOrReplace(new utils.MoveTransformComponent(mouse.getComponent(Transform).position, food.getComponent(Transform).position, 4))
    cat.addComponentOrReplace(new utils.MoveTransformComponent(cat.getComponent(Transform).position, food.getComponent(Transform).position, 4))
  }))
 
//create mouse
const mouse = new Entity()
mouse.addComponent(new SphereShape())
mouse.getComponent(SphereShape).withCollisions = false
mouse.addComponent(new Transform({ position: new Vector3(1 + Math.random() * 14, 0, 1 + Math.random() * 14), scale: new Vector3(0.5, 0.5, 0.5) }))
mouse.addComponent(new utils.TriggerComponent(
    new utils.TriggerBoxShape(Vector3.One(), Vector3.Zero()),
    mouseLayer, 
    catLayer,
    () => {
    mouse.getComponent(Transform).position = new Vector3(1 + Math.random() * 14, 0, 1 + Math.random() * 14)
    mouse.addComponentOrReplace(new utils.MoveTransformComponent(mouse.getComponent(Transform).position, food.getComponent(Transform).position, 4))
  }))
 
//create cat
const cat = new Entity()
cat.addComponent(new BoxShape())
cat.getComponent(BoxShape).withCollisions = false
cat.addComponent(new Transform({ position: new Vector3(1 + Math.random() * 14, 0, 1 + Math.random() * 14) }))
cat.addComponent(new utils.TriggerComponent(
    new utils.TriggerBoxShape(Vector3.One(), Vector3.Zero()), 
    catLayer
))
 
//set initial movement for mouse and cat
mouse.addComponentOrReplace(new utils.MoveTransformComponent(
    mouse.getComponent(Transform).position, 
    food.getComponent(Transform).position, 
    4
))
cat.addComponentOrReplace(new utils.MoveTransformComponent(
    cat.getComponent(Transform).position, 
    food.getComponent(Transform).position, 
    4
))
 
//add entities to engine
engine.addEntity(food)
engine.addEntity(mouse)
engine.addEntity(cat)


// gold fish by Novelo, GooglePoly

